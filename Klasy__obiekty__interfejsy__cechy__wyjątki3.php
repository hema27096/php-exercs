<?php
class Knapsack{
private $capacity;
private $items;

public __construct Knapsack($capacity){
    $this->capacity=capacity;
    $this->items=array();

}

public function getItems(){
    return $this->items;
}

private function getTotalWeight(){
    $totalweight=0;
    foreach ($this->items as $item){
        $totalweight+=$item->getWeight();
    }
    return $totalWeight;
}

public function addItem($item){
if($this->getTotalWeight()+$item->getWeight()<= $this->capacity){
    $this->items[]= $item;
    return true;
}
return false;
}

public funtion deleteItem ($item){
$index =array_search($item, $this->items);
if ($index !==false){
    array_splice($this->items, $index, 1);
    return true;
}
return false;
}
}


class TwoKnapsack extends Knapsack {
    private $capacity2;
    private $secondItems;

    public __construct TwoKnapsack($capacity2, $secondItems){
        parent:: __construct($capacity);
        $this->capacity2=$capacity2;
        $this->secondItems=array();
    }

public function getSecondItems(){
    return $this->secondItems;
}
public function addItem($item){
    if(parent::getTotalWeight()+$item->getWeight()<=$this->capacity){
        return parent::addItem($item);
    }
    elseif ($this->getTotalWeight()+ $item->getWeight()<= $this->capacity2){
        $this->secondItems[]=$item;
        return true;
    }
    return false;
}
public function deleteItem($item){
    if (parent::deleteItem($item)){
        return true;
    }
    $index = array_search($item, $this->secondItems);
    if ($index !==false){
        array_splice($this->secondItems, $index, 1);
        return true;
    }
    return false;
}
    


}


?>