<?php class Point {
private $x;
private $y;

public function __construct($x, $y){
$this->x=$x;
$this->y=$y;
}

public function getX(){
return $this->x;
}
public function getY(){
    return $this->y;
}

public function setX($x){
$this->x=$x;
}
public function setY($y){
$this->y=$y;
}
public function shift(Point $vector){
    $this->x += $vector->getX();
    $this->y += $vector->getY();
}
public function distance(Point $point){
 $xDistance= $point->getX() - $this->x;
 $yDistance=$point->getY() - $this->y;   
 return sqrt(pow($xDistance, 2)+ pow($yDistance,2));
}
public function __toString(){
    return "({$this->x}, {$this->y})";
}

}
?>