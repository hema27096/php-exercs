<?php

interface Figure {
    public function area();
}

interface FlatFigure extends Figure{
    public function perimeter();
}
interface SpatialFigure extends Figure{
    public function volume;
}
class Rectangle implements Figure {
private $length;
private $width;

public function __construct($length, $width){
    if($length <=0 || $width<=0){
        throw new Exception ("Długość i szerokość prostokąta muszą być większe od 0");
    }
    $this->length=$length;
    $this->width=$width;
}
public function area(){
    return $this->length*$this->width;
}
public function perimeter(){
    return 2* ($this->length+$this->width);
}
class Ball implements SpatialFigure{
    private $radius;
    public function __construct($radius){
        if ($radius<=0){
            throw new Exception("Promien kuli musi byc wiekszy od 0");
        }
        $this->radius=$radius;  
    }
    public function area(){
        return 4*pi()*pow($this->radius,2);
    }
public function volume(){
return (4/3)*pi()*pow($this->radius,3);
}
}

try {
    $rectangle = new Rectangle(5,8);
    echo "Pole prostokąta to:".$rectangle->area(). "<br>";
    echo "Obwod prostokąta to:".$rectangle->perimeter()."<br>";
}
catch (Exception $e){
    echo "Błąd: ". $e->getMessage()."<br>";
}
try {
    $ball = new Ball(3);
    echo "Pole powierzchni kuli to:".$ball.area()."<br>";
    echo "Objętość kuli to:".$ball.volume()."<br>";
}
catch (Exception $e){
    echo "Błąd:".getMessage()."<br>";
}


?>