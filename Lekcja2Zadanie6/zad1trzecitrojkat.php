<?php
function drukuj_trojkat($n) {
    for ($row = 1; $row <= $n; $row++) {
        for ($column = 0; $column < $n - $row; $column++) {
            echo " ";
        }
        for ($column = 0; $column < $row; $column++) {
            echo "*";
        }
        echo "\n";
    }
}
drukuj_trojkat(5);
?>