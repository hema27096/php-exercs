<?php

function transponowanatablica($tablica) {
    $transponowana = array();
    foreach ($tablica as $row => $columns) {
    foreach ($columns as $column => $value) {
        $transponowana[$column][$row] = number_format((float) $value, 1, '.', '');
    }
}
return $transponowana;
}

$tablica = array(array(1.0, 2.0, 3.0), array(4.0, 5.0, 6.0));

echo "Tablica: \n";
foreach ($tablica as $row) {
    echo implode(' ', $row) . "\n";
}
echo "Transponowana tablica: \n";
$transponowana = transponowanatablica($tablica);
foreach ($transponowana as $row) {
    echo implode(' ', $row). "\n";
}

?>