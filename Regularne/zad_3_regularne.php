<?php function sprawdzsamogloski($string) {
    $wzor = '/^([aeiouy]).*\1$/i';
    return preg_match($wzor, $string);
}

$string = "anananassaaa";
if (sprawdzsamogloski($string)) {
    echo "Slowo ma taka sama samogloske na poczatku i koncu";
} else {
    echo "Slowo nie ma takiej samej samogloski na poczatku i koncu";
}
?>