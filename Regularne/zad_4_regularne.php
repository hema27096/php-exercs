<php function sprawdzemail($email) {
    $wzor = "/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/";
    return preg_match($wzor, $email);
}

$email = "lmielewczyk@pjwstk.edu.pl";
if (sprawdzemail($email)) {
    echo "Adres e-mail jest poprawny";
} else {
    echo "Adres e-mail jest niepoprawny";
}
?>