<?php

function sumaliczb($str) {
  $matches = [];
  preg_match_all('/\d+/', $str, $matches);
  $sum = 0;
  foreach ($matches[0] as $number) {
    $sum += (int) $number;
  }
  return $sum;
}

$str = "1. Ala(10) ma 23 koty";
$sum = sumaliczb($str);
echo "Suma liczb w napisie \"$str\" to $sum.";

?>





