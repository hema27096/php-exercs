<?php

function zmiendate($data) {
  $miesiace = array(
    '01' => 'styczeń',
    '02' => 'luty',
    '03' => 'marzec',
    '04' => 'kwiecień',
    '05' => 'maj',
    '06' => 'czerwiec',
    '07' => 'lipiec',
    '08' => 'sierpień',
    '09' => 'wrzesień',
    '10' => 'październik',
    '11' => 'listopad',
    '12' => 'grudzień'
  );
  $regex = '/^([01]?[0-9])[\.\/-]([0-3]?[0-9])[\.\/-]([0-9]{4})$/';
  if (preg_match($regex, $data, $matches)) {
    $dzien = (int) $matches[2];
    $miesiac = $miesiace[$matches[1]];
    $rok = (int) $matches[3];
    return "$dzien $miesiac $rok";
  } else {
    return "Nieprawidłowy format";
  }
}


$data1 = "8.3.2023";
$data2 = "8-3-2023";
$data3 = "8/3/2023";
$nowadata1 = zmiendate($data1);
$nowadata2 = zmiendate($data2);
$nowadata3 = zmiendate($data3);
echo "$data1 -> $nowadata1\n";
echo "$data2 -> $nowadata2\n";
echo "$data3 -> $nowadata3\n";

?>