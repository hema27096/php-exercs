<?php function samogloski($string) {
    $wzor = '/[aeiouy]{2}/i';
    return preg_match($wzor, $string);
}

$string = "ahbabuuu";
if (samogloski($string)) {
    echo "Slowo ma dwie samogłoski obok siebie";
} else {
    echo "Slowo nie ma dwoch samogłosek obok siebie";
}
?>