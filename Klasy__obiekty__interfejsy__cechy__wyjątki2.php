<?php
abstract class Item{
private $name;
private $weight;

public function __construct($name="Unknown", $weight="0"){
    $this->name=$name;
    $this->weight=$weight;
}

public function getName(){
    return $this->name;
}
public function setName($name){
    $this->name=$name;
}
public function getWeight(){
return $this->weight;
}
public function setWeight($weight){
    $this->weight=$weight;
}


}
class Juice extends Item{
    private $liters;
    public function __construct($name, $weight, $liters){
        parent::__construct($name, $weight);
        $this->liters=$liters;
    }
    public function getLiters(){
        return $this->liters;
    }
    public function setLiters($liters){
        $this->liters= $liters;
    }
}

$item = new Item();
echo "Nazwa".$item->getName()."\n";
echo "Waga".$item->getWeight()."\n";

$item->setName("PRODUKT 1");
$item->setWeight(10);
echo "Nazwa".$item->getName()."\n";
echo "Waga".$item->getWeight()."\n";

$juice= new Juice("Pomaranczowy", 1, 2);
echo "Nazwa soku".$juice->getName()."\n";
echo "Waga soku".$juice->getWeight()."\n";
echo "Sok ma".$juice->getLiters()."litrow"."\n";


?>